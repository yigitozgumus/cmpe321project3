<html>
    <head>
      <link rel="stylesheet" href="style.css">
      <title>Hospital Patient Page</title>
    </head>
    <body>

        <?php
			// Starting session
			session_start();

            $servername = "localhost";
            $username = "root";
            $password = "root";
            $dbname = "CMPEHOSPITAL";

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);

            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }else{
              if (!$_SESSION["login_user"]) {
            		echo "You need to <a hrefn='patient_signin.php'>login</a> to access this page.";
            	} else {
                // Update the record
                $month=$_POST['month'];
                $dt=$_POST['dt'];
                $year=$_POST['year'];
                $date_value="$year-$month-$dt";
                $time_value =$_POST['time'];
                $sql = "UPDATE appointments SET doctor_id = '" . $_POST["subcat"] . "', p_userName = '" .
                $_SESSION["login_user"] . "', Date = '" . $date_value . "', Time = '" . $time_value
                . "', branch_id = '" . $_POST["cat"] . "' WHERE appointment_ID =" . $_SESSION["id"];
                $dateCheck=TRUE;
                $timeCheck=TRUE;
                $doctorCheck=TRUE;
                $check_query = "SELECT * FROM appointments";
                $checkResult = $conn->query($check_query);
                while($row = $checkResult->fetch_assoc()){
                  if($_SESSION["login_user"]=== $row["p_userName"]){
                    $idCheck=TRUE;
                  }
                }
                $checkResult = $conn->query($check_query);

                  while($row = $checkResult->fetch_assoc()){
                    if($time_value=== $row["Time"]){
                      $timeCheck=FALSE;
                    }else if($date_value=== $row["Date"]){
                      $dateCheck =FALSE;
                    }else if($doctorCheck === $row["doctor_id"]){
                      $doctorCheck =FALSE;
                    }
                  }
                  if($timeCheck && $dateCheck && $doctorCheck){
                    if ($conn->query($sql) === TRUE) {
                      echo "<h2 id=\"h01\" align=\"center\">The Appointment has been edited.</h2>";
                      ?>
                      <div class="wrapper">
                        <button class="back back2" onclick="parent.location='appointment_list.php'">List of Appointments</button>
                      </div>
                      <div class="wrapper">
                      <button class="back back1" onclick="parent.location='patient_homepage.php'">Admin Page</button>
                      </div>
                     <?php
                    } else {
                        echo "Error deleting record: " . $conn->error;
                    }
                 }else{
                   echo "<h2 id=\"h01\" align=\"center\">There is a conflict with the appointment you edited Please choose another time.</h2>";
                   ?>
                   <div class="wrapper">
                  <button class="back back2" onclick="parent.location='appointment_list.php'">Appointment list</button>
                  <button class="back back2" onclick="parent.location='create_appointment_form.php'">Create an Appointment</button>
                  </div>
                  <?php
                 }


            }
          }
            $conn->close();
        ?>

    </body>
</html>
