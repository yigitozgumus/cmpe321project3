<html>
    <head>
        <title>Your Appointments</title>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>

        <?php

        	session_start();

            $servername = "localhost";
            $username = "root";
            $password = "root";
            $dbname = "CMPEHOSPITAL";

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);

            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }else{

				if (!$_SESSION['login_user']){
					echo "You need to <a href = 'login.php'>login</a> to access this page.";
				}else{
					// List records
					$sql = "SELECT appointments.appointment_ID, appointments.Date, appointments.Time, doctors.first_name, doctors.last_name, patients.p_firstname, patients.p_lastname,branches.branch FROM appointments INNER JOIN patients ON appointments.p_username = patients.p_username INNER JOIN branches ON appointments.branch_id = branches.branch_id INNER JOIN doctors ON appointments.doctor_id=doctors.doctor_id WHERE patients.p_username = '" . $_SESSION["login_user"] . "' ";
					$result = $conn->query($sql);

					if ($result->num_rows > 0) {
						?>
            <h2 id="h01" align="center">Your Appointments</h2>
						<table border = 1 align=center id="logtable">
							<tr>
                <th>Operations</th>
								<th>Date</th>
								<th>Time</th>
                <th>Branch</th>
								<th>Doctor First Name</th>
								<th>Doctor Last Name</th>
								<th>Patient First Name</th>
								<th>Patient Last Name</th>
							</tr>
						<?php

						// output data of each row
						while($row = $result->fetch_assoc()) {
							?>
							<tr>
                <td>
                  <a href = "cancel_appointment_form.php?id=<?php echo $row["appointment_ID"]; ?>"><img src = "img/trash.png" width="20" height="20"  alt = "Delete" /></a>
									<a href = "edit_appointment_form.php?id=<?php echo $row["appointment_ID"]; ?>"><img src = "img/edit.png" width="20" height="20"  alt = "Edit" /></a>
                </td>
								<td><?php echo $row["Date"]; ?></td>
								<td><?php echo $row["Time"]; ?></td>
                <td><?php echo $row["branch"]; ?></td>
								<td><?php echo $row["first_name"]; ?></td>
								<td><?php echo $row["last_name"]; ?></td>
								<td><?php echo $row["p_firstname"]; ?></td>
								<td><?php echo $row["p_lastname"]; ?></td>
							</tr>
							<?php
						}

						?>
						</table>
						<?php
					} else {
            ?>
						<h2 id="h01" align="center">You have no Appointments in the system</h2>
            <?php
					}
          ?>
            <div class="wrapper">
              <button class="back back2" onclick="parent.location='patient_homepage.php'">Patient Homepage</button>
         </div>
          <?php

				}
            }
            $conn->close();
        ?>

    </body>
</html>
