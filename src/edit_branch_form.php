
 <!DOCTYPE html>
 <html>
 <head>
     <link rel="stylesheet" href="style.css">
     <title>Hospital Admin Page</title>
 </head>
 <body>
   <?php

			// Starting session
			session_start();

      $servername = "localhost";
      $username = "root";
      $password = "root";
      $dbname = "CMPEHOSPITAL";

            // Create connection
            $connection = new mysqli($servername, $username, $password, $dbname);

            // Check connection
            if ($connection->connect_error) {

                die("Connection failed: " . $connection->connect_error);
            }else {
            	if (!$_SESSION["login_user"]) {
            		echo "You need to <a hrefn='admin_signin.php'>login</a> to access this page.";
            	} else {
                // Fetch the record
                $sql = "SELECT branch_id, branch FROM branches WHERE branch_id = " . $_GET['id'];
                $result = $connection->query($sql);

                // If the record actually exists
                if ($result->num_rows > 0) {
                    ?>
                    <form action="edit_branch_result.php" method="post">
                      <?php
                      $row = $result->fetch_assoc();
                      ?>
                      <fieldset>
                      <legend > <h2 id="h02">Branch Info Edit Form</h2></legend>
                      <b id="h03">ID</b><br>
                      <p><input type="text" name="ID" id="id" value = "<?php echo $row["branch_id"] ?>" readonly ></p>
                      <b id="h03">Branch Name</b><br>
                      <p><input type="text" name="branchname" id="branchname" value = "<?php echo $row["branch"] ?>"></p>
                      <button class="submit" type="submit" name="login" value="login" > Submit</button>
                      </fieldset>
                  </form>

            <?php
          }
        }
      }
            $connection->close();
        ?>
 </body>

 </html>
