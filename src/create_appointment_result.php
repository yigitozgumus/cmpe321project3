<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="style.css">
    <title>Hospital Patient Page</title>

</head>
<body>
  <?php

     // Starting session
     session_start();

     $servername = "localhost";
     $username = "root";
     $password = "root";
     $dbname = "CMPEHOSPITAL";

           // Create connection
           $connection = new mysqli($servername, $username, $password, $dbname);

           // Check connection
           if ($connection->connect_error) {

               die("Connection failed: " . $connection->connect_error);
           }else {
             if (!$_SESSION["login_user"]) {
               echo "You need to <a hrefn='admin_signin.php'>login</a> to access this page.";
             } else {
               if(isset($_POST['cat']) && isset($_POST['subcat']) && isset($_POST['time']) && isset($_POST['month']) && isset($_POST['dt']) && isset($_POST['year'])){
                 //Preparing all the ingredients
                 $month=$_POST['month'];
                 $dt=$_POST['dt'];
                 $year=$_POST['year'];
                 $date_value="$year-$month-$dt";
                 $time_value =$_POST['time'];
                 $patient = $_SESSION['login_user'];
                 $doctor = $_POST['subcat'];
                 $branch = $_POST['cat'];
                 $query = "INSERT INTO appointments(doctor_id,p_userName,Date,Time,branch_id) " . "VALUES('" . $doctor
                 . "', '" . $patient . "', '" . $date_value . "', '" . $time_value . "', '" . $branch . "')";

                 $dateCheck=TRUE;
                 $timeCheck=TRUE;
                 $doctorCheck=TRUE;
                 $check_query = "SELECT * FROM appointments";
                 $checkResult = $connection->query($check_query);
                 while($row = $checkResult->fetch_assoc()){
                   if($_SESSION["login_user"]=== $row["p_userName"]){
                     $idCheck=TRUE;
                   }
                 }
                 $checkResult = $connection->query($check_query);

                   while($row = $checkResult->fetch_assoc()){
                     if($time_value=== $row["Time"]){
                       $timeCheck=FALSE;
                     }else if($date_value=== $row["Date"]){
                       $dateCheck =FALSE;
                     }else if($doctorCheck === $row["doctor_id"]){
                       $doctorCheck =FALSE;
                     }
                   }
                   if($timeCheck && $dateCheck && $doctorCheck){
                     if($connection->query($query) === TRUE){
                       echo "<h2 id=\"h01\" align=\"center\">New Appointment has been added to the system</h2>";
                       ?>
                       <div class="wrapper">
                   		<button class="back back2" onclick="parent.location='appointment_list.php'">Appointments</button>
                   		</div>
                       <div class="wrapper">
                   		<button class="back back1" onclick="parent.location='patient_homepage.php'">Patient Homepage</button>
                   		</div>
                      <?php
                    }else{
                      echo "Query is wrong?";
                    }
                  }else{
                    echo "<h2 id=\"h01\" align=\"center\">There is a conflict with the appointment you created. Please choose another time.</h2>";
                    ?>
                    <div class="wrapper">
                   <button class="back back2" onclick="parent.location='appointment_list.php'">Appointment list</button>
                   <button class="back back2" onclick="parent.location='create_appointment_form.php'">Create an Appointment</button>
                   </div>
                   <?php
                  }


               }else{
                 echo "Appointment parameters are not posted.";
               }
           ?>

           <?php
         }
       }
           $connection->close();
       ?>
</body>

</html>
