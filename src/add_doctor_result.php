<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="style.css">
    <title>Hospital Doctor Page</title>
</head>
<body>
  <?php

     // Starting session
     session_start();

     $servername = "localhost";
     $username = "root";
     $password = "root";
     $dbname = "CMPEHOSPITAL";

           // Create connection
           $conn = new mysqli($servername, $username, $password, $dbname);

           // Check connection
           if ($conn->connect_error) {

               die("Connection failed: " . $conn->connect_error);
           }else {
           	if (!$_SESSION["login_user"]) {
           		echo "You need to <a hrefn='admin_signin.php'>login</a> to access this page.";
           	} else {
             // create the query and insert the record
             $query = "INSERT INTO doctors(first_name, last_name, branch) " .
                    "VALUES('" . $_POST['firstname'] . "', '" . $_POST['lastname'] .
                    "', '" . $_POST['branch'] . "')";

            if ($conn->query($query) === TRUE) {
                   echo "<h2 id=\"h01\" align=\"center\">New Doctor has been added to the stuff</h2>";
                   ?>
                   <div class="wrapper">
               		<button class="back back2" onclick="parent.location='doctor_list.php'">Doctors</button>
                  <button class="back back2" onclick="parent.location='add_doctor_form.php'">Add Doctor</button>
               		</div>
                   <div class="wrapper">
               		<button class="back back1" onclick="parent.location='admin_homepage.php'">Admin Page</button>
               		</div>
                  <?php
               } else {
                   echo "Error updating record: " . $conn->error;
               }
           }
         }
           $conn->close();
       ?>
</body>

</html>
