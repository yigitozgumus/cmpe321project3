
 <!DOCTYPE html>
 <html>
 <head>
     <link rel="stylesheet" href="style.css">
     <title>Hospital Doctor Page</title>
 </head>
 <body>
   <?php

			// Starting session
			session_start();

      $servername = "localhost";
      $username = "root";
      $password = "root";
      $dbname = "CMPEHOSPITAL";

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);

            // Check connection
            if ($conn->connect_error) {

                die("Connection failed: " . $conn->connect_error);
            }else {
            	if (!$_SESSION["login_user"]) {
            		echo "You need to <a hrefn='admin_signin.php'>login</a> to access this page.";
            	} else {
            ?>
            <form  action="add_doctor_result.php" method="post">
                <fieldset>
                <legend > <h2 id="h02">Doctor Addition Form</h2></legend>
                <b id="h03">First Name</b><br>
                <p><input type="text" name="firstname" id="firstname" ></p>
                <b id="h03">Last Name</b><br>
                <p><input type="text" name="lastname" id="lastname" ></p>
                <b id="h03">Branch</b><br>
                <?php
                $query = "SELECT * FROM branches";
          			$result = $conn->query($query);
                 ?>
                 <p><select name='branch' value=''></p><option>Select a Branch</option>
         				<?php
         					while($row = $result->fetch_assoc()){
         						echo "<option value='$row[branch_id]'>$row[branch]</option>";
         					}
         				 ?>
         				<p></select></p>
                <button class="submit" type="submit" name="login" value="login" > Submit</button>
                </fieldset>
            </form>
            <?php
          }
        }
            $conn->close();
        ?>
 </body>

 </html>
