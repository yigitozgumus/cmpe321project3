<html>
    <head>
      <link rel="stylesheet" href="style.css">
      <title>Hospital Patient Page</title>
    </head>
    <body>

        <?php
			// Starting session
			session_start();

            $servername = "localhost";
            $username = "root";
            $password = "root";
            $dbname = "CMPEHOSPITAL;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);

            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }else{
              if (!$_SESSION["login_user"]) {
            		echo "You need to <a hrefn='patient_signin.php'>login</a> to access this page.";
            	} else {
                // Update the record
                $sql = "DELETE FROM appointments WHERE appointment_id = " . $_POST['ID'];

                if ($conn->query($sql) === TRUE) {
                  echo "<h2 id=\"h01\" align=\"center\">The Appointment has been cancelled.</h2>";
                  ?>
                  <div class="wrapper">
                    <button class="back back2" onclick="parent.location='appointment_list.php'">List of Appointments</button>
                  </div>
                  <div class="wrapper">
                  <button class="back back1" onclick="parent.location='patient_homepage.php'">Admin Page</button>
                  </div>
                 <?php
                } else {
                    echo "Error deleting record: " . $conn->error;
                }
            }
          }
            $conn->close();
        ?>

    </body>
</html>
