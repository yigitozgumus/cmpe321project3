<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="style.css">
    <title>Hospital Doctor Page</title>
</head>
<body>
  <?php

     // Starting session
     session_start();

     $servername = "localhost";
     $username = "root";
     $password = "root";
     $dbname = "CMPEHOSPITAL";

           // Create connection
           $conn = new mysqli($servername, $username, $password, $dbname);

           // Check connection
           if ($conn->connect_error) {

               die("Connection failed: " . $conn->connect_error);
           }else {
           	if (!$_SESSION["login_user"]) {
           		echo "You need to <a hrefn='admin_signin.php'>login</a> to access this page.";
           	} else {
             // create the query and insert the record
             $query = "UPDATE doctors SET first_name = '" . $_POST['firstname'] . "', last_name = '" . $_POST['lastname'] .
                    "', branch = '" . $_POST['branch'] .  "' WHERE doctor_id = " . $_POST['ID'];

            if ($conn->query($query) === TRUE) {
                   echo "<h2 id=\"h01\" align=\"center\">Doctor info has been edited</h2>";
                   ?>
                   <div class="wrapper">
               		<button class="back back1" onclick="parent.location='admin_homepage.php'">Admin Page</button>
               		</div>
                  <?php
               } else {
                   echo "Error updating record: " . $conn->error;
               }
           }
         }
           $conn->close();
       ?>
</body>

</html>
