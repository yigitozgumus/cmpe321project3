
<html>
	<head>
		<title>Admin Homepage</title>
		<link rel="stylesheet" href="style.css">
	</head>
<body>
<?php
//Admin session start
session_start();
$servername    = "localhost";
$user          = "root";
$user_password = "root";
$db            = "CMPEHOSPITAL";

// Create the connection
$connection = new mysqli($servername, $user, $user_password, $db);

//Check connection
if ($connection->connect_error) {
	die("Connection Failed: ".$connection->connect_error);
} else {
	if (!$_SESSION["login_user"]) {
		echo "You need to <a hrefn='admin_signin.php'>login</a> to access this page.";
	} else {
			$query = "SELECT * FROM branches";
			$result = $connection->query($query);

		?>
		<h2 id="h01" align="center">Welcome <?php echo $_SESSION["login_user"];?></h2>
		<h2 id="h01" align="center">Admin Operations</h2>
		<div class="wrapper" style="margin-top: 15px; text-align: center;">
	 <h4 id="h04">You can access EDIT and DELETE operations from the List buttons. </h4>
	</div>
		<div class="wrapper">
		<button class="back back2" onclick="parent.location='doctor_list.php'">List of Doctors</button>
		<button class="back back2" onclick="parent.location='add_doctor_form.php'">Add Doctor</button>
		</div>
		<div class="wrapper">
		<button class="back back2" onclick="parent.location='branch_list.php'">List of Branches</button>
		<button class="back back2" onclick="parent.location='add_branch_form.php'">Add Branch</button>
		</div>
		<div class="wrapper" style="margin-top: 15px; text-align: center;">
		<h4 id="h04" >Future Appointments Report </h4>
		</div>
		<form method=post name=futureaps action='future_appointments_form.php' align="center">
			<fieldset>

				<select name='future' value=''><option>Select a Branch</option>
				<?php
					while($row = $result->fetch_assoc()){
						echo "<option value='$row[branch_id]'>$row[branch]</option>";
					}
				 ?>
				 <option value="all">All<opttion>
				</select>

				<button class="submit" type="submit" name="submit" value="submit" > Submit</button>
			</fieldset>
		</form>
		<div class="wrapper" style="margin-top: 15px; text-align: center;">
		<h4 id="h04">Past Appointments Report </h4>
	  </div>
		<form method=post name='pastaps' action='past_appointments_form.php' align="center">
			<fieldset>
				<select name='past' value=''><option>Select a Branch</option>
				<?php
					$result = $connection->query($query);
					while($row = $result->fetch_assoc()){
						echo "<option value='$row[branch_id]'>$row[branch]</option>";
					}
				 ?>
				 <option value="all">All<opttion>
				</select>
				<button class="submit" type="submit" name="login" value="login" > Submit</button>
			</fieldset>
		</form>
		<div class="wrapper">
		<button class="back back1" onclick="parent.location='logout.php'">Log Out</button>
		</div>
		<?php
	}

}
$connection->close();
?>
</body>
</html>
