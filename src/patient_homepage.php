<html>
	<head>
		<title>
			Patient Homepage
		</title>
		<link rel="stylesheet" href="style.css">
	</head>
	<body>
<?php
//Admin session start
session_start();
$servername    = "localhost";
$user          = "root";
$user_password = "root";
$db            = "CMPEHOSPITAL";

// Create the connection
$conn = new mysqli($servername, $user, $user_password, $db);

//Check connection
if ($conn->connect_error) {
	die("Connection Failed: ".$conn->connect_error);
} else {
	if (!$_SESSION["login_user"]) {
		echo "You need to <a href = 'patient_signin.php'>login</a> to access this page.";
	} else {
		?>
		<h2 id="h01" align="center">Welcome <?php echo $_SESSION["login_user"];?></h2>
		<h2 id="h01" align="center">Patient Operations</h2>
		<div class="wrapper">
			<button class="back back2" onclick="parent.location='appointment_list.php'">List of Appointments</button>
			<button class="back back2" onclick="parent.location='create_appointment_form.php'">Create new appointment</button>
		</div>
		<div class="wrapper">
			<button class="back back1" onclick="parent.location='logout.php'">Log Out</button>
		</div>
		<?php
	}

}
$conn->close();
?>
</body>
</html>
