<html>
    <head>
      <link rel="stylesheet" href="style.css">
      <title>Hospital Branch Page</title>
    </head>
    <body>

        <?php
			session_start();

            $servername = "localhost";
            $username = "root";
            $password = "root";
            $dbname = "CMPEHOSPITAL";

            // Create connection
            $connection = new mysqli($servername, $username, $password, $dbname);

            // Check connection
            if ($connection->connect_error) {
                die("Connection failed: " . $connection->connect_error);
            }else{

          if (!$_SESSION["login_user"]) {
            echo "You need to <a hrefn='admin_signin.php'>login</a> to access this page.";
          } else {
					?>
          <div class="wrapper">
      		<button class="back back2" onclick="parent.location='add_branch_form.php'">Add a new Branch</button>
      		</div>
					<?php

					// List records
					$sql = "SELECT branch_id,branch FROM branches";
					$result = $connection->query($sql);

					if ($result->num_rows > 0) {
						?>
						<table border = 1 align=center id="logtable">
							<tr>
								<th>Operations</th>
								<th>ID</th>
								<th>Branch</th>
						<?php
						while($row = $result->fetch_assoc()) {
							?>
							<tr>
								<td>
									<a href = "delete_branch_form.php?id=<?php echo $row["branch_id"]; ?>"><img src = "img/trash.png" width="20" height="20"  alt = "Delete" /></a>
									<a href = "edit_branch_form.php?id=<?php echo $row["branch_id"]; ?>"><img src = "img/edit.png" width="20" height="20"  alt = "Edit" /></a>
								</td>
								<td><?php echo $row["branch_id"]; ?></td>
								<td><?php echo $row["branch"]; ?></td>
							</tr>
							<?php
						}
						?>
						</table>
						<?php
					} else {
						echo "There are no branches.";
					}
					?>
        </div>
         <div class="wrapper">
        <button class="back back2" onclick="parent.location='admin_homepage.php'">Admin Page</button>
        </div>
          <?php
				    }
          }
            $connection->close();
        ?>

    </body>
</html>
