<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="style.css">
    <title>Hospital Admin Page</title>
</head>
<body>
  <?php

     // Starting session
     session_start();

     $servername = "localhost";
     $username = "root";
     $password = "root";
     $dbname = "CMPEHOSPITAL";

           // Create connection
           $connection = new mysqli($servername, $username, $password, $dbname);

           // Check connection
           if ($connection->connect_error) {

               die("Connection failed: " . $connection->connect_error);
           }else {
           	if (!$_SESSION["login_user"]) {
           		echo "You need to <a hrefn='admin_signin.php'>login</a> to access this page.";
           	} else {
             // create the query and insert the record
             $query = "UPDATE branches SET branch = '" . $_POST['branchname'] .  "' WHERE branch_id = " . $_POST['ID'];

            if ($connection->query($query) === TRUE) {
                   echo "<h2 id=\"h01\" align=\"center\">Branch info has been edited</h2>";
                   ?>
                   <div class="wrapper">
               		<button class="back back1" onclick="parent.location='admin_homepage.php'">Admin Page</button>
               		</div>
                  <?php
               } else {
                   echo "Error updating record: " . $connection->error;
               }
           }
         }
           $connection->close();
       ?>
</body>

</html>
