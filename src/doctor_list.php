<html>
    <head>
      <link rel="stylesheet" href="style.css">
      <title>Hospital Doctor Page</title>
    </head>
    <body>

        <?php
			session_start();

            $servername = "localhost";
            $username = "root";
            $password = "root";
            $dbname = "CMPEHOSPITAL";

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);

            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }else{

          if (!$_SESSION["login_user"]) {
            echo "You need to <a hrefn='admin_signin.php'>login</a> to access this page.";
          } else {
					?>

					<?php

					// List records
					$sql = "SELECT doctors.doctor_id,doctors.first_name,doctors.last_name,branches.branch FROM doctors INNER JOIN branches ON doctors.branch= branches.branch_id";
					$result = $conn->query($sql);

					if ($result->num_rows > 0) {
						//echo $result->num_rows;
						?>
            <h2 id="h01" align="center">Doctor List</h2>
						<table border = 1 align=center id="logtable">
							<tr>
								<th>Operations</th>
								<th>ID</th>
								<th>First Name</th>
								<th>Last Name</th>
								<th>branch</th>
						<?php
						// output data of each row
						while($row = $result->fetch_assoc()) {
							?>
							<tr>
								<td>
									<a href = "delete_doctor_form.php?id=<?php echo $row["doctor_id"]; ?>"><img src = "img/trash.png" width="20" height="20"  alt = "Delete" /></a>
									<a href = "edit_doctor_form.php?id=<?php echo $row["doctor_id"]; ?>"><img src = "img/edit.png" width="20" height="20"  alt = "Edit" /></a>
								</td>
								<td><?php echo $row["doctor_id"]; ?></td>
								<td><?php echo $row["first_name"]; ?></td>
								<td><?php echo $row["last_name"]; ?></td>
								<td><?php echo $row["branch"]; ?></td>
							</tr>
							<?php
						}
						?>
						</table>
						<?php
					} else {
						echo "There is no doctor";
					}
					?>
        </div>
         <div class="wrapper">
        <button class="back back2" onclick="parent.location='admin_homepage.php'">Admin Page</button>
        <button class="back back2" onclick="parent.location='add_doctor_form.php'">Add a new Doctor</button>
        </div>
          <?php
				    }
          }
            $conn->close();
        ?>

    </body>
</html>
