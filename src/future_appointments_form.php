<html>
    <head>
        <title>Future Appointments</title>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>

        <?php

        	session_start();

            $servername = "localhost";
            $username = "root";
            $password = "root";
            $dbname = "CMPEHOSPITAL";

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);

            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }else{

				if (!$_SESSION['login_user']){
					echo "You need to <a href = 'login.php'>login</a> to access this page.";
				}else{
          ?>
          <h2 id="h01" align="center">Future Appointments </h2>
          <table border = 1 align=center id="logtable">
            <tr>
              <th>ID</th>
              <th>Date</th>
              <th>Time</th>
              <th>Branch</th>
              <th>Doctor First Name</th>
              <th>Doctor Last Name</th>
              <th>Patient First Name</th>
              <th>Patient Last Name</th>
            </tr>
          <?php
          error_reporting(0);
          if($_POST["future"] === "all"){
              $branch_query = "SELECT * FROM branches";
              $result = $conn->query($branch_query);
              while($row1 = $result->fetch_assoc()){
                $result2 = $conn->query("CALL all_apps_future()");
                if ($result2->num_rows > 0) {
      						// output data of each row
      						while($row2 = $result2->fetch_assoc()) {
      							?>
      							<tr>
      								<td><?php echo $row2["appointment_ID"]; ?></td>
      								<td><?php echo $row2["Date"]; ?></td>
      								<td><?php echo $row2["Time"]; ?></td>
                      <td><?php echo $row2["branch"]; ?></td>
      								<td><?php echo $row2["first_name"]; ?></td>
      								<td><?php echo $row2["last_name"]; ?></td>
      								<td><?php echo $row2["p_firstname"]; ?></td>
      								<td><?php echo $row2["p_lastname"]; ?></td>
      							</tr>
      							<?php
      						}
              }
            }
          }else{
            // List records
  					$result = $conn->query("CALL future_apps('" . $_POST['future'] . "')");
            if ($result->num_rows > 0) {
              // output data of each row
              while($row = $result->fetch_assoc()) {
                ?>
                <tr>
                  <td><?php echo $row["appointment_ID"]; ?></td>
                  <td><?php echo $row["Date"]; ?></td>
                  <td><?php echo $row["Time"]; ?></td>
                  <td><?php echo $row["branch"]; ?></td>
                  <td><?php echo $row["first_name"]; ?></td>
                  <td><?php echo $row["last_name"]; ?></td>
                  <td><?php echo $row["p_firstname"]; ?></td>
                  <td><?php echo $row["p_lastname"]; ?></td>
                </tr>
                <?php
              }
          }else {
            ?>
            <h2 id="h01" align="center">There are no Appointments</h2>
            <?php
					}
          }
			       ?>
						</table>
            <div class="wrapper">
              <button class="back back2" onclick="parent.location='admin_homepage.php'">Admin Homepage</button>
         </div>
          <?php

				}
            }
            $conn->close();
        ?>

    </body>
</html>
