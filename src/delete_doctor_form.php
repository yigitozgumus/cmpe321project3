<html>
    <head>
      <link rel="stylesheet" href="style.css">
      <title>Hospital Doctor Page</title>
    </head>
    <body>

        <?php
			// Starting session
			session_start();

            $servername = "localhost";
            $username = "root";
            $password = "root";
            $dbname = "CMPEHOSPITAL";

            // Create connection
            $connection = new mysqli($servername, $username, $password, $dbname);

            // Check connection
            if ($connection->connect_error) {
                die("Connection failed: " . $connection->connect_error);
            }else{
              if (!$_SESSION["login_user"]) {
            		echo "You need to <a hrefn='admin_signin.php'>login</a> to access this page.";
            	} else {
                // Fetch the record
                $sql = "SELECT doctor_id,first_name, last_name, branch FROM doctors WHERE doctor_id = " . $_GET['id'];
                $result = $connection->query($sql);

                // If the record actually exists
                if ($result->num_rows > 0) {
                    //echo $results->num_rows;
                    ?>
                    <form action="delete_doctor_result.php" method="post">
                    <?php

                    // Get the data
                    $row = $result->fetch_assoc();
                    ?>
                        <h2 id="h01" align="center">Are you sure you want to delete the following doctor's record ?</h2>
                        <fieldset>
                        <legend > <h2 id="h02">Doctor Deletion Form</h2></legend>
                        <b id="h03">ID</b><br>
                        <p><input type="text" name="ID" id="id" value = "<?php echo $row["doctor_id"] ?>" readonly ></p>
                        <b id="h03">First Name</b><br>
                        <p><input type="text" name="firstname" id="firstname" value = "<?php echo $row["first_name"] ?>"readonly></p>
                        <b id="h03">Last Name</b><br>
                        <p><input type="text" name="lastname" id="lastname" value = "<?php echo $row["last_name"] ?>"readonly></p>
                        <b id="h03">Branch</b><br>
                        <p><input type="text" name="branch" id="branch" value = "<?php echo $row["branch"] ?>"readonly></p>
                        <button class="submit" type="submit" name="login" value="login" > Submit</button>
                        </fieldset>
                    </form>
                    <?php
                } else {
                    ?>
                    <h2 id="h01" align="center">Record doesn't exist.</h2>
                    <?php
                }
            }
          }
            $connection->close();
        ?>
    </body>
</html>
