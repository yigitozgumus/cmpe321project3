<html>
    <head>
      <link rel="stylesheet" href="style.css">
      <title>Hospital Patient Page</title>
    </head>
    <body>

        <?php
			// Starting session
			session_start();

            $servername = "localhost";
            $username = "root";
            $password = "root";
            $dbname = "CMPEHOSPITAL";

            // Create connection
            $connection = new mysqli($servername, $username, $password, $dbname);

            // Check connection
            if ($connection->connect_error) {
                die("Connection failed: " . $connection->connect_error);
            }else{
              if (!$_SESSION["login_user"]) {
            		echo "You need to <a hrefn='patient_signin.php'>login</a> to access this page.";
            	} else {
                // Fetch the record
                $sql = "SELECT appointments.appointment_ID, appointments.Date, appointments.Time, doctors.first_name, doctors.last_name, patients.p_firstname, patients.p_lastname,branches.branch FROM appointments INNER JOIN patients ON appointments.p_username = patients.p_username INNER JOIN branches ON appointments.branch_id = branches.branch_id INNER JOIN doctors ON appointments.doctor_id=doctors.doctor_id WHERE appointments.appointment_ID = '" . $_GET["id"] . "'";
                $result = $connection->query($sql);

                // If the record actually exists
                if ($result->num_rows > 0) {
                    //echo $results->num_rows;
                    ?>
                    <form action="cancel_appointment_result.php" method="post">
                    <?php
                    // Get the data
                    $row = $result->fetch_assoc();
                    $doctor = $row["first_name"] . " " . $row["last_name"];
                    $patient = $row["p_firstname"] . " " .  $row["p_lastname"]
                    ?>
                        <h4 id="h04">Are you sure you want to delete the following appointment? </h4>
                        <fieldset>
                        <legend > <h2 id="h02">Appointment Cancellation Form</h2></legend>
                        <b id="h03">ID</b><br>
                        <p><input type="text" name="ID" id="id" value = "<?php echo $row["appointment_ID"] ?>" readonly ></p>
                        <b id="h03">Branch</b><br>
                        <p><input type="text" name="branch" id="branch" value = "<?php echo $row["branch"] ?>"readonly></p>
                        <b id="h03">Doctor</b><br>
                        <p><input type="text" name="doctor" id="doctor" value = "<?php echo $doctor  ?>"readonly></p>
                        <b id="h03">Patient Name</b><br>
                        <p><input type="text" name="doctor" id="doctor" value = "<?php echo $patient  ?>"readonly></p>
                        <b id="h03">Time</b><br>
                        <p><input type="text" name="time" id="time" value = "<?php echo $row["Time"] ?>"readonly></p>
                        <b id="h03">Date</b><br>
                        <p><input type="text" name="time" id="time" value = "<?php echo $row["Date"] ?>"readonly></p>
                        <button class="submit" type="submit" name="login" value="login" > Submit</button>
                        </fieldset>
                    </form>
                    <?php
                } else {
                    echo "Record does not exist";
                }
            }
          }
            $connection->close();
        ?>
    </body>
</html>
