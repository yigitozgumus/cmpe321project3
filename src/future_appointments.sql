CREATE DEFINER=`root`@`localhost` PROCEDURE `future_apps`(IN `branch` INT(11))
  NO SQL
BEGIN
  SELECT appointments.appointment_ID,appointments.p_userName,appointments.Date,appointments.Time,doctors.first_name,doctors.last_name,branches.branch,patients.p_firstname,patients.p_lastname FROM appointments
  INNER JOIN doctors on appointments.doctor_id = doctors.doctor_id
  INNER JOIN branches on appointments.branch_id= branches.branch_id
  INNER JOIN patients on appointments.p_userName=patients.p_username
  WHERE appointments.branch_id=branch AND appointments.Date >= CURRENT_DATE
END
