<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="style.css">
    <title>Hospital Patient Page</title>
    <SCRIPT language=JavaScript>
    function reload(form)
    {
      var val=form.cat.options[form.cat.options.selectedIndex].value;
      self.location='create_appointment_form.php?cat=' + val ;
}

</script>
</head>
<body>
  <?php
    @$cat=$_GET['cat'];
     // Starting session
     session_start();

     $servername = "localhost";
     $username = "root";
     $password = "root";
     $dbname = "CMPEHOSPITAL";

           // Create connection
           $connection = new mysqli($servername, $username, $password, $dbname);

           // Check connection
           if ($connection->connect_error) {

               die("Connection failed: " . $connection->connect_error);
           }else {
             if (!$_SESSION["login_user"]) {
               echo "You need to <a hrefn='admin_signin.php'>login</a> to access this page.";
             } else {
               ?>
               <h2 id="h01" align="center">Create an Appointment</h2>
               <?php
               // Create arrays for doctors and branches
               $query1 = "SELECT * FROM branches";
               $query2 = "SELECT * FROM doctors WHERE doctors.branch='" . $cat . "'";
               ?>
               <form method=post name=f1 action='create_appointment_result.php'>
                 <fieldset>
                   <b id="h03">Branch</b><br>
                   <?php
               echo "<p><select name='cat' onchange=\"reload(this.form)\"><option value=''>Select Branch</option></p>";
               foreach($connection->query($query1) as $noticia2){
                 if($noticia2['branch_id']==@$cat){echo "<option selected value='$noticia2[branch_id]'>$noticia2[branch]</option>"."<BR>";}
                 else{echo  "<option value='$noticia2[branch_id]'>$noticia2[branch]</option>";}
               }
               ?>
               <p></select></p>
               <b id="h03">Doctors</b><br>
               <p><select name='subcat'><option value=''>Select Doctor</option></p>
                <?php
               foreach ($connection->query($query2) as $noticia) {
              echo  "<option value='$noticia[doctor_id]'>$noticia[first_name] $noticia[last_name]</option>";
               }
               ?>
               <p></select></p>
               <b id="h03">Time</b><br>
               <p><select name=time value=''></p>
               <?php
                 $starttime = '08:00';
                 $endtime = '17:00';
                 $time = new DateTime($starttime);
                 $interval = new DateInterval('PT5M');
                 $temptime = $time->format('H:i');
                 $i =1;
                 do{
                   echo "<option value='". $temptime ."'>" . $temptime."</option>" ;
                   $time->add($interval);
                   $temptime = $time->format('H:i');
                   $i++;
                 }while($temptime !== $endtime);
                 ?>
               <p></select></p>

               <b id="h03">Months</b><br>
               <p><select name=month value=''></p>
               <option value='01'>January</option>
               <option value='02'>February</option>
               <option value='03'>March</option>
               <option value='04'>April</option>
               <option value='05'>May</option>
               <option value='06'>June</option>
               <option value='07'>July</option>
               <option value='08'>August</option>
               <option value='09'>September</option>
               <option value='10'>October</option>
               <option value='11'>November</option>
               <option value='12'>December</option>
               <p></select></p>

               <b id="h03">Days</b><br>
               <p><select name=dt value=''></p>
               <option value='01'>01</option>
               <option value='02'>02</option>
               <option value='03'>03</option>
               <option value='04'>04</option>
               <option value='05'>05</option>
               <option value='06'>06</option>
               <option value='07'>07</option>
               <option value='08'>08</option>
               <option value='09'>09</option>
               <option value='10'>10</option>
               <option value='11'>11</option>
               <option value='12'>12</option>
               <option value='13'>13</option>
               <option value='14'>14</option>
               <option value='15'>15</option>
               <option value='16'>16</option>
               <option value='17'>17</option>
               <option value='18'>18</option>
               <option value='19'>19</option>
               <option value='20'>20</option>
               <option value='21'>21</option>
               <option value='22'>22</option>
               <option value='23'>23</option>
               <option value='24'>24</option>
               <option value='25'>25</option>
               <option value='26'>26</option>
               <option value='27'>27</option>
               <option value='28'>28</option>
               <option value='29'>29</option>
               <option value='30'>30</option>
               <option value='31'>31</option>
               <p></select></p>

               <b id="h03">Year</b><br>
               <p><select name=year value=''></p>
                 <?php
                   $starttime = 2000;
                   $endtime = 2199;
                   do{
                     echo "<option value='". $starttime ."'>" . $starttime."</option>" ;

                     $starttime++;
                   }while($starttime !== $endtime);
                   ?>
                 <p></select></p>

               <button class="submit" type="submit" name="login" value="login" > Submit</button>
               </fieldset>
               </form>
               <div class="wrapper">
              <button class="back back1" onclick="parent.location='patient_homepage.php'">Patient Homepage</button>
              </div>

           <?php
         }
       }
           $connection->close();
       ?>
</body>
</html>
